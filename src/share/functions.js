export const paginate = (data, page, perPage) => {
    return data.slice((page - 1) * perPage, (page) * perPage)
};
export const sortFunc = (data, orderBy, direction) => {
    let tempData = [...data]
    if (orderBy && ['DESC', 'ASC'].includes(direction))
        return tempData.sort((a, b) => {
            const sortA = a[orderBy];
            const sortB = b[orderBy];
            if (direction === 'DESC') {
                if (sortA < sortB) return 1;
                if (sortA > sortB) return -1;
                return 0
            } else {
                if (sortA < sortB) return -1;
                if (sortA > sortB) return 1;
                return 0
            }
        })
    else
        return tempData
};

export const filterFunc = (data, filters) => {
    if (Object.keys(filters).length)
        return data.filter(item => {
            for (let key in filters) {
                if (filters.hasOwnProperty(key) && filters[key] && !item[key].includes(filters[key])) {
                    return false
                }
            }
            return true;
        });
    else
        return data
}