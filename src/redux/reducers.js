import {removeFavoriteItem,addFavoriteItem} from "./constants";

function rootReducer(state = [], action) {
    switch (action.type) {
        case addFavoriteItem:
            return [
                ...state,
                action.item
            ]
        case removeFavoriteItem:
            return state.filter(item => item.id !== action.item.id)
        default:
            return state;
    }
}

export default rootReducer;
