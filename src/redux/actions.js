import {removeFavoriteItem, addFavoriteItem} from "./constants";

export function addItem(item) {
    return {
        type: addFavoriteItem,
        item
    }
}

export function removeItem(item) {
    return {
        type: removeFavoriteItem,
        item
    }
}