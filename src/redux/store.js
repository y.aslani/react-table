import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers";
import { persistStore, persistCombineReducers } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import logger from "redux-logger";

const MODE = "DEV";
const ConfigureStore = () => {
    const config = {
        key: "root",
        storage,
        debug: true, //should be false for production mode
    };
    const Store = createStore(
        persistCombineReducers(config, {
            rootReducer,
        }),
        MODE === "DEV" ? applyMiddleware(thunk, logger) : applyMiddleware(thunk)
    );
    const Persistor = persistStore(Store);
    return { Store, Persistor };
};
export const { Store, Persistor } = ConfigureStore();
const store = createStore(rootReducer);

export default store;
