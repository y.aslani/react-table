import Axios from "axios";
import urls from "./urls";

const TIMEOUT = 60000;

const instance = Axios.create({
    baseURL: "/",
    timeout: TIMEOUT,
});

const request = (name, params = null, payload = null) => {
    return new Promise(function (resolve, reject) {
        instance.request({
            url: urls[name].url,
            method: urls[name].method,
            data: payload,
        })
            .then((data) => {
                resolve(data.data);
            })
            .catch((error) => {
                let err = {
                    message: error.response.data ? error.response.data.message : "",
                    status: error.response.status,
                    code: error.response.data ? error.response.data.code : "",
                };
                reject(err);
            });
    });
};


export default request;
