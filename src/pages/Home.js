import React, {useState, useEffect} from 'react';
import styles from "../styles/pages/Home.module.scss"
import request from "../services/axios";
import {useDispatch, useSelector} from "react-redux";
import {addItem, removeItem} from "../redux/actions";
import {filterFunc} from "../share/functions";
import Table from "../components/Table";
import TableFilters from "../components/TableFilters";
import {useSearchParams, useLocation} from 'react-router-dom';

const Home = () => {
    const headers = [
        {text: "نشان شده", value: "selectable",},
        {text: "نام تغییر دهنده", value: "name",},
        {text: "تاریخ", value: "date", sortable: true, className: 'nowrap'},
        {text: "نام آگهی", value: "title",},
        {text: "فیلد", value: "field"},
        {text: "مقدار قدیمی", value: "old_value"},
        {text: "مقدار جدید", value: "new_value"},
    ]
    const filterInputs = [
        {text: "نام تغییر دهنده", value: "name", type: 'text'},
        {text: "تاریخ", value: "date", type: 'text'},
        {text: "نام آگهی", value: "title", type: 'text'},
        {text: "فیلد", value: "field", type: 'text'},]


    const storedFavoriteItems = useSelector((state) => state.rootReducer);
    const dispatch = useDispatch();
    const [searchParams, setSearchParams] = useSearchParams();
    let {search} = useLocation();
    const [state, setState] = useState({
        data: [],
        favoriteItems: [],
        filteredItems: [],
        filtersProps: {},
        sortProps: {}
    })

    const addToFavorite = (item) => {
        let tempState = {...state}
        item.selected ? dispatch(removeItem(item)) : dispatch(addItem(item))
        item.selected = !item.selected;
        setState(tempState)
    }

    const setSortQuery = (query) => {
        setState((prev) => ({...prev, sortProps: query}))
        setSearchParams({...state.filtersProps, ...query})
    }

    const filterList = (filters) => {
        let tempState = {...state}
        tempState.filtersProps = filters
        tempState.filteredItems = filterFunc(state.data, filters)
        setSearchParams({...filters, ...tempState.sortProps})
        setState(tempState)
    }

    const getData = () => {
        request('getList')
            .then(data => {
                setState({...state, data: data})
            })
            .catch(err => {
            })
    }

    useEffect(() => {
        //add favorite list to main list
        let tempState = {...state}
        storedFavoriteItems.forEach(item => {
            let foundIndex = tempState.data.findIndex(i => i.id === item.id)
            if (foundIndex !== -1)
                tempState.data[foundIndex].selected = true
        })

        //read query string sort and filters
        const query = new URLSearchParams(search);

        //get filter property from query string
        filterInputs.forEach(input => {
            if (query.get(input.value))
                tempState.filtersProps[input.value] = query.get(input.value)
        })
        //get sort property from query string
        if (query.get('direction') && query.get('orderBy'))
            tempState.sortProps = {direction: query.get('direction'), orderBy: query.get('orderBy')};

        //filter data
        tempState.filteredItems = filterFunc(state.data, tempState.filtersProps)

        setState(tempState)
    }, [state.data])

    useEffect(() => {
        getData()
    }, [])

    return (
        <div className={styles.container}>
            <TableFilters
                inputs={filterInputs}
                filterList={filterList}
                initialFilters={state.filtersProps}
            />
            <Table
                headers={headers}
                items={state.filteredItems}
                addToFavorite={addToFavorite}
                setQuery={setSortQuery}
                initialSort={state.sortProps}
            />
        </div>
    )
}
export default Home