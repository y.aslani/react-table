import React, {useState, useEffect} from "react";
import styles from "../styles/components/TableFilters.module.scss"

const TableFilters = ({initialFilters, filterList, inputs}) => {

    const [state, setState] = useState({})

    const handleChange = (label, value) => {
        setState({...state, [label]: value});
    }

    const submitForm = (e) => {
        e.preventDefault();
        let query = {}
        inputs.forEach((item) => {
            if (state[item.value])
                query[item.value] = state[item.value]
        });
        filterList(query)
    }

    useEffect(() => {
        let tempState = {...state};
        inputs.forEach((item) => {
            tempState[item.value] = initialFilters[item.value] ? initialFilters[item.value] : '';
        });
        setState(tempState);
    }, [JSON.stringify(initialFilters)])


    return (
        <form className={styles.filter} onSubmit={(e) => submitForm(e)}>
            <div className={styles.inputs}>
                {inputs.map(item =>
                    <div key={item.value} className={styles.item}>
                        <label>
                            {item.text}:
                            <input
                                value={state[item.value] ? state[item.value] : ''}
                                name={item.value}
                                type={item.type}
                                onChange={(e) => handleChange(item.value, e.target.value)}
                            />
                        </label>
                    </div>
                )}
            </div>

            <div className={styles.btn}>
                <button type="submit">
                    اعمال فیلتر
                </button>
            </div>
        </form>
    )
}

export default TableFilters