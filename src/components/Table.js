import React, {useState, useMemo, useEffect} from "react"
import PropTypes from 'prop-types'
import {paginate, sortFunc} from "../share/functions";
import styles from "../styles/components/Table.module.scss"
import {BsChevronRight, BsChevronLeft, BsStarFill, BsStar, BsChevronUp, BsChevronDown} from "react-icons/bs";

const Table = ({
                   headers,
                   items = [],
                   perPage = 10,
                   addToFavorite,
                   initialSort,
                   setQuery
               },
) => {
    const totalPage = useMemo(() => calculateTotalPage(items.length, perPage), [items.length, perPage]);

    const [state, setState] = useState({
        page: 1,
        direction: '',
        orderBy: '',
        sortIcon: '',
        sortedData: [],
    })

    const goToPage = (step) => {
        setState({...state, page: state.page + (step === 'next' ? 1 : -1)})
    }

    const changeListOrder = (orderBy) => {
        let tempState = {...state}
        if (orderBy && orderBy !== tempState.orderBy)
            tempState.direction = ''
        tempState.orderBy = orderBy
        if (!tempState.direction) {
            tempState.direction = 'DESC'
            tempState.sortIcon = <BsChevronDown/>
        } else if (tempState.direction === 'DESC') {
            tempState.direction = 'ASC'
            tempState.sortIcon = <BsChevronUp/>
        } else {
            tempState.direction = ''
            tempState.sortIcon = ''
            tempState.orderBy = ''
        }
        tempState.page = 1

        let query = {}
        if (tempState.direction) {
            query = {direction: tempState.direction, orderBy: orderBy};
        }
        tempState.sortedData = sortFunc(items, tempState.orderBy, tempState.direction)
        setQuery(query)
        setState(tempState)
    }

    useEffect(() => {
        let tempState = {...state}
        if (initialSort.direction && initialSort.orderBy) {
            tempState.direction = initialSort.direction
            tempState.orderBy = initialSort.orderBy
            tempState.sortIcon = tempState.direction === 'DESC' ? <BsChevronDown/> : <BsChevronUp/>
        }
        tempState.sortedData = sortFunc(items, tempState.orderBy, tempState.direction)
        tempState.page = 1
        setState(tempState)
    }, [items])

    return (
        <>
            <button className={styles.sortBtn} onClick={() => changeListOrder('date')}>
                <span>مرتب سازی بر اساس تاریخ</span>
                {state.orderBy === 'date' ? state.sortIcon : ''}
            </button>
            <table>
                <thead>
                <tr>
                    {headers.map((header, index) => {
                            if (header.sortable)
                                return (
                                    <th key={header.value}>
                                        <div className={styles.sortable} onClick={() => changeListOrder(header.value)}>
                                            <span>{header.text}</span>
                                            {state.orderBy === header.value ? state.sortIcon : ''}
                                        </div>
                                    </th>
                                ); else
                                return (
                                    <th key={header.value}>
                                        {header.text}
                                    </th>
                                )
                        }
                    )}
                </tr>
                </thead>
                <tbody>
                {paginate(state.sortedData, state.page, perPage).map((item) =>
                    <tr key={item.id} className={item.selected ? styles.active : ''}>
                        {headers.map(header => {
                                if (header.value === 'selectable')
                                    return (
                                        <td key={header.value}>
                                            <button
                                                className={styles.btn}
                                                onClick={() => addToFavorite(item)}>
                                                {item.selected ?
                                                    <BsStarFill size={20} color="#FFED01"/> :
                                                    <BsStar size={20} color="#FFED01"/>
                                                }
                                            </button>
                                        </td>
                                    )
                                else
                                    return (
                                        <td data-label={header.text} key={header.value} className={header.className}>
                                            {item[header.value]}
                                        </td>
                                    )
                            }
                        )}
                    </tr>
                )}
                </tbody>
            </table>
            <div className={styles.paginate}>
                <button disabled={state.page === totalPage} onClick={() => goToPage('next')}>
                    <BsChevronRight size={15}/>
                </button>
                <span className={styles.page}>
                    {`${state.page}/${totalPage}`}
                </span>
                <button disabled={state.page === 1} onClick={() => goToPage('prev')}>
                    <BsChevronLeft size={15}/>
                </button>
            </div>
        </>
    )
}

const calculateTotalPage = (totalItem, perPage) => {
    return Math.ceil(totalItem / perPage);
};
Table.propTypes = {
    headers: PropTypes.array.isRequired
}
export default Table